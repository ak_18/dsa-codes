//Chef and Good subsequences sept19long

#include <iostream>
#include<bits/stdc++.h>
#include<string>
#include<stdlib.h>
#include<math.h>
#include<vector>
#include<queue>
#include<stack>
#include<utility>
#include<numeric>
#include<ctime>
#include<set>
using namespace std;
#define fastio ios_base::sync_with_stdio(false);cin.tie(NULL);cout.tie(NULL);
#define     ll          long long int 
int main() 
{
	fastio
    ll n,k,i,j,x,y,q,ans=1;
    cin >> n >> q;
    ll a[n];
    for(i=0;i<n;i++)        cin >> a[i];
    ll freq[8005]={0};
    for(i=0;i<n;i++)        freq[a[i]]+=1;
    vector<ll> v;
    for(i=0;i<8005;i++){
        if(freq[i]>0)       v.push_back(freq[i]);
    }
    k=v.size();
    ll arr[2][k]={0};
    for(j=0;j<k;j++)        {arr[0][j]=v[j]%1000000007;    ans=(ans+v[j])%1000000007;}
    //cout << ans << "   ";
    for(j=0;j<=k-1;j++)
    {   ll sum=0;
        for(i=j+1;i<k;i++)       sum+= arr[0][i];
        arr[1][j]=(arr[0][j]*(sum%1000000007))%1000000007;
        ans=(ans+arr[1][j])%1000000007;
    }
    //cout << ans << "   ";
    for(i=2;i<k;i++)
    {  
        for(j=0;j<=k-i-1;j++)
        {   ll sum=0;
            for(x=j+1;x<=k-i;x++)    sum=(sum+arr[1][x])%1000000007;
            arr[1][j]=(arr[0][j]*(sum%1000000007))%1000000007;
            ans=(ans+(arr[1][j]%1000000007))%1000000007;
        }
        if(i==q-1)    break;
        
    }
    cout << ans;
	return 0;
}
